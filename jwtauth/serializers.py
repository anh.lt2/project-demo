from rest_framework import serializers
from django.contrib.auth.models import User


class RegisterSerializer(serializers.Serializer):
    email    = serializers.EmailField(max_length=50)
    username = serializers.CharField(max_length=50)
    password = serializers.CharField(max_length=150, write_only=True)
    # class Meta:
    #     model = User
    #     fields = ('email', 'password', 'username')
    def validate(self, data):
        email_exist = User.objects.filter(email=data["email"]).exists()
        if email_exist:
            raise serializers.ValidationError({'email': ('email already exists')})
        if User.objects.filter(username=data["username"]).exists():
            raise serializers.ValidationError({'username': ('username already exists')})
        return data
    
    def create(self, data):
        user = User.objects.create(
            # first_name=data["first_name"],
            # last_name=data["last_name"],
            email=data["email"],
            username=data["username"],
            password=data["password"],
        )
        user.set_password(data['password'])
        return user

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=50)
    password = serializers.CharField(max_length=150, )





    
    # class Meta:
    #     model = User
    #     fields = ['username', 'password', 'email']
    # def validate(self, data):
    #     # print(email)
    #     # email_exist = User.objects.filter(email=data['username']).first()
    #     # print('seeeeeeee', email_exist)
    #     # if email_exist:
    #     #     raise serializers.ValidationError({'username': ('username already exists')})
    #     # if User.objects.filter(username=data["password"]).exists():
    #     #     raise serializers.ValidationError({'password': ('password already exists')})
    #     return data
    
    # def post(self, data):


