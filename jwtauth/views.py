from django.contrib.auth.models import User
from .serializers import RegisterSerializer, LoginSerializer
from rest_framework.views import Response, APIView
from rest_framework import status, exceptions
from django.contrib.auth.hashers import make_password, check_password
# from django.contrib.auth import login
# from .authentication import create_access_token
# from rest_framework.authtoken.serializers import AuthTokenSerializer

# Create your views here.

class RegisterAPIView(APIView):
    def post(self, request):
        serializer = RegisterSerializer(data=request.data)
        print(serializer)
        # if not serializer.is_valid():
        #     return Response({
        #         'error_message': 'Register Failed',
        #         'errors_code': 400,
        #     }, status=status.HTTP_400_BAD_REQUEST)
        # serializer.save()
        # return Response('Register successful!', status=status.HTTP_201_CREATED)
        
        if serializer.is_valid():
            serializer.validated_data['password'] = make_password(serializer.validated_data['password'])
            serializer.save()
            return Response('Register successful!', status=status.HTTP_201_CREATED)
        else:
            return Response({
                 'error_message': 'Register Failed',
                 'errors_code': 400,
             }, status=status.HTTP_400_BAD_REQUEST)

# class LoginAPIView(APIView):

#     def post(self, request):
#         username = request.data.get('username')
#         password = request.data.get('password')
#         if not username and not password:
#             return Response({
#                 "message": "Require"
#             })
#         try:
#             user = User.objects.get(username=username)
#         except User.DoesNotExist:
#             raise exceptions.APIException({
#                 "message": "Not found"
#             })
#         if not user.check_password(password):
#             return Response({
#                 "message": "Invalid password"
#             })
#         return Response({
#             "message": "oke"
#         })













        # params = (
        #     self.request.query_params
        #     if len(self.request.data) == 0 
        #     else self.request.data
        # )
        # serializer = LoginSerializer(data=request.data)
        # if not serializer.is_valid():
        #     return Response({
        #         'error_message': 'This user has already exist!',
        #         'errors_code': 400,
        #     }, status=status.HTTP_400_BAD_REQUEST)
        
        # uname = params.get('username')
        # pwd =  params.get('password')
        # print(uname, pwd)
        # print(User.objects.all())
        check_user = User.objects.get(username=uname)
        print(check_user)
        # access = create_access_token(check_user.id)
        # print(access)
        # if check_user:
        #     request.session['user'] = uname
        return Response({
            'results': check_user,
            'message' : 'login success'
        })
        

        # login(request, serializer)

